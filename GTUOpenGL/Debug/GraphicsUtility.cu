#include "GraphicsUtility.cuh"

__global__ void normalCalculator(Surface* surfaces, Normal *normals) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	Surface surface = surfaces[i];

	Vertex a, b;
	double l;

	a.x = surface.third.x - surface.second.x;
	a.y = surface.third.y - surface.second.y;
	a.z = surface.third.z - surface.second.z;

	b.x = surface.first.x - surface.second.x;
	b.y = surface.first.y - surface.second.y;
	b.z = surface.first.z - surface.second.z;

	normals[i].x = (a.y * b.z) - (a.z * b.y);
	normals[i].y = (a.z * b.x) - (a.x * b.z);
	normals[i].z = (a.x * b.y) - (a.y * b.x);


	// Normalize (divide by root of dot product)
	l = sqrt(normals[i].x * normals[i].x + normals[i].y * normals[i].y + normals[i].z * normals[i].z);
	normals[i].x /= l;
	normals[i].y /= l;
	normals[i].z /= l;
}

void GraphicsUtility::prepareNormals() {
	computeNormals(Point3D(14, -83, -57), Size(15, 20, 40), 0, GraphicsParameters::GEO_BUILDING_STANDARD); // 0
	computeNormals(Point3D(14, -83, 90), Size(15, 20, 40), 0, GraphicsParameters::GEO_BUILDING_STANDARD); // 1
	computeNormals(Point3D(14, 180, -62), Size(15, 20, 40),0,GraphicsParameters::GEO_BUILDING_STANDARD); // 2
	computeNormals(Point3D(14, 158, -41), Size(15, 40, 20), 0, GraphicsParameters::GEO_BUILDING_STANDARD); // 3
	computeNormals(Point3D(14, 280, -62), Size(15, 20, 40), 0, GraphicsParameters::GEO_BUILDING_STANDARD); // 4
	computeNormals(Point3D(14, 420, -20), Size(15, 20, 40), 0, GraphicsParameters::GEO_BUILDING_STANDARD); // 5
	computeNormals(Point3D(0, -82, 135), Size(40, 130), 1, GraphicsParameters::GEO_GROUND); // 6
	computeNormals(Point3D(0, -107, 90), Size(7, 80), 1, GraphicsParameters::GEO_GROUND); // 7
	computeNormals(Point3D(0, -43, 90), Size(45, 230), 1, GraphicsParameters::GEO_GROUND); // 8
	computeNormals(Point3D(0, 233, 51), Size(35, 300),1, GraphicsParameters::GEO_GROUND); // 9
	computeNormals(Point3D(0, 190, 220), Size(550, 40), 1, GraphicsParameters::GEO_GROUND); // 10
	computeNormals(Point3D(0, 447, 50), Size(35, 300), 1, GraphicsParameters::GEO_GROUND); // 11
	computeNormals(Point3D(0, 190, -120), Size(550, 40), 1, GraphicsParameters::GEO_GROUND); // 12
	computeNormals(Point3D(0, 99, 100), Size(240, 200), 1, GraphicsParameters::GEO_GROUND); // 13
	computeNormals(Point3D(0, 366, -60), Size(130, 80), 1, GraphicsParameters::GEO_GROUND); // 14
	computeNormals(Point3D(0, 350, 119), Size(200, 200), 1, GraphicsParameters::GEO_GROUND); // 15
	computeNormals(Point3D(0, 171, 0), Size(550, 40), 1, GraphicsParameters::GEO_GROUND); // 16
	computeNormals(Point3D(0, -43, -60), Size(40, 80), 1, GraphicsParameters::GEO_GROUND); // 17
	computeNormals(Point3D(0, 50, -60), Size(200, 80), 1, GraphicsParameters::GEO_GROUND); // 18
	computeNormals(Point3D(0, 256, -60), Size(10, 80), 1, GraphicsParameters::GEO_GROUND); // 19
}

void GraphicsUtility::prepareTextures() {
	loadTexture("grass.png"); // 0

	// -------------- ELECTRONIC ENGINEERING -------------------
	loadTexture("elektro_muh_on_cephe.jpg"); // 1
	loadTexture("elektro_muh_arka_cephe.jpg"); // 2
	loadTexture("elektro_muh_yan_cephe.jpg"); // 3
	// --------------------------------------------------------

	// ------------- COMPUTER ENGINEERING ---------------------
	loadTexture("bilmuh_on_cephe.jpg"); // 4
	loadTexture("bilmuh_arka_cephe.jpg"); // 5
	loadTexture("bilmuh_sol_cephe.jpg"); // 6
	loadTexture("bilmuh_sag_cephe.jpg"); // 7
	// --------------------------------------------------------

	// ------------- LIBRARY ---------------------
	loadTexture("kutuphane_on_cephe.jpg"); // 8
	loadTexture("kutuphane_sol_cephe.jpg"); // 9
	loadTexture("kutuphane_sag_cephe.jpg"); // 10
	loadTexture("kutuphane_arka_cephe.jpg"); // 11
	loadTexture("kutuphane_arka_cephe_2.jpg"); // 12
	loadTexture("kutuphane_arka_cephe_3.jpg"); // 13
	// --------------------------------------------------------

	// ------------- MOLECULAR BIOLOGY ---------------------
	loadTexture("molekuler_on_cephe.jpg"); // 14
	loadTexture("molekuler_arka_cephe.jpg"); // 15
	loadTexture("molekuler_sol_cephe.jpg"); // 16
	loadTexture("molekuler_sag_cephe.jpg"); // 17
	// --------------------------------------------------------

	// ------------- ENVIRONMENTAL ENGINEERING ---------------------
	loadTexture("cevre_on_cephe.jpg"); // 18
	loadTexture("cevre_arka_cephe.jpg"); // 19
	loadTexture("cevre_sol_cephe.jpg"); // 20
	loadTexture("cevre_sag_cephe.jpg"); // 21
	// --------------------------------------------------------

	loadTexture("roof.jpg"); // 22
	loadTexture("bilmuh_floor.jpg"); // 23
	loadTexture("skybox_back.jpg"); // 24
	loadTexture("skybox_bottom.jpg"); // 25
	loadTexture("skybox_front.jpg"); // 26
	loadTexture("skybox_right.jpg"); // 27
	loadTexture("skybox_top.jpg"); // 28
	loadTexture("skybox_left.jpg"); // 29
}

void GraphicsUtility::drawGround(Size& size, Point3D& coordinate, double polygonPeriod, int textureIndex, int groundIndex) {
	int currentNormal = 0;
	Normal *normals = buildingNormals.at(groundIndex);
	Texture texture = textureImages.at(textureIndex);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture.textureId);

	glBegin(GL_QUADS);

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			glNormal3f(-normals[currentNormal].x, -normals[currentNormal].y, -normals[currentNormal].z);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f - i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, 1, 1	
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f - i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, 1, -1
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f - i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, -1, -1
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f - i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, -1, 1
			++currentNormal;
		}
	}

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			glNormal3f(-normals[currentNormal].x, -normals[currentNormal].y, -normals[currentNormal].z);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f + i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, 1, 1
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f + i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, 1, -1
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f + i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, -1, -1
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f + i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, -1, 1
			++currentNormal;
		}
	}

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			glNormal3f(-normals[currentNormal].x, -normals[currentNormal].y, -normals[currentNormal].z);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f - i + coordinate.getY(), 1.0f - j + coordinate.getZ()); // -1, 1, 1
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f - i + coordinate.getY(), -1.0f - j + coordinate.getZ()); // -1, 1, -1
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f - i + coordinate.getY(), -1.0f - j + coordinate.getZ()); // -1, -1, -1
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f - i + coordinate.getY(), 1.0f - j + coordinate.getZ()); // -1, -1, 1
			++currentNormal;
		}
	}

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			glNormal3f(-normals[currentNormal].x, -normals[currentNormal].y, -normals[currentNormal].z);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f + i + coordinate.getY(), 1.0f - j + coordinate.getZ()); // -1, 1, 1
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f + i + coordinate.getY(), -1.0f - j + coordinate.getZ()); // -1, 1, -1
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f + i + coordinate.getY(), -1.0f - j + coordinate.getZ()); // -1, -1, -1
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f + i + coordinate.getY(), 1.0f - j + coordinate.getZ()); // -1, -1, 1
			++currentNormal;
		}
	}
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void GraphicsUtility::drawOutline(Point3D& coordinate, Size& size, Color *color, double polygonPeriod) {
	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			glBegin(GL_LINES);
			glColor3f(0, 1.0f, 0);
			glVertex3f(1.0f + i, 1.0f, -1.0f + j); // 1, 1, -1
			glVertex3f(-1.0f + i, 1.0f, -1.0f + j); // -1, 1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, 1.0f, -1.0f + j); // -1, 1, -1
			glVertex3f(-1.0f + i, 1.0f, 1.0f + j); // -1, 1, 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, 1.0f, 1.0f + j); // -1, 1, 1
			glVertex3f(1.0f + i, 1.0f, 1.0f + j); // 1, 1, 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(1.0f + i, 1.0f, 1.0f + j); // 1, 1, 1
			glVertex3f(1.0f + i, 1.0f, -1.0f + j); // 1, 1, -1
			glEnd();
		}
	}

	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			glBegin(GL_LINES);
			glColor3f(1.0f, 0, 0);
			glVertex3f(1.0f + i, -size.getZSize() - 1, 1.0 + j); // 1, -zSize - 1, 1
			glVertex3f(-1.0f + i, -size.getZSize() - 1, 1.0f + j); // -1, -zSize - 1, 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, -size.getZSize() - 1, 1.0f + j); // -1, -zSize - 1, 1
			glVertex3f(-1.0f + i, -size.getZSize() - 1, -1.0f + j); // -1, -zSize - 1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, -size.getZSize() - 1, -1.0f + j); // -1, -zSize - 1, -1
			glVertex3f(1.0f + i, -size.getZSize() - 1, -1.0f + j); // 1, -zSize - 1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(1.0f + i, -size.getZSize() - 1, -1.0f + j); // 1, -zSize - 1, -1
			glVertex3f(1.0f + i, -size.getZSize() - 1, 1.0 + j); // 1, -zSize - 1, 1
			glEnd();
		}
	}


	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getZSize(); j += polygonPeriod) {
			glBegin(GL_LINES);
			glColor3f(0, 0, 1.0f);
			glVertex3f(1.0f + i, 1.0f - j, size.getYSize() + 1); // 1, 1, ySize + 1
			glVertex3f(-1.0f + i, 1.0f - j, size.getYSize() + 1); // -1, 1, ySize + 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, 1.0f - j, size.getYSize() + 1); // -1, 1, ySize + 1
			glVertex3f(-1.0f + i, -1.0f - j, size.getYSize() + 1); // -1, -1, ySize + 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, -1.0f - j, size.getYSize() + 1); // -1, -1, ySize + 1
			glVertex3f(1.0f + i, -1.0f - j, size.getYSize() + 1); // 1, -1, ySize + 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(1.0f + i, -1.0f - j, size.getYSize() + 1); // 1, -1, ySize + 1
			glVertex3f(1.0f + i, 1.0f - j, size.getYSize() + 1); // 1, 1, ySize + 1
			glEnd();
		}
	}


	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getZSize(); j += polygonPeriod) {
			glBegin(GL_LINES);
			glColor3f(1.0f, 1.0f, 0);
			glVertex3f(1.0f + i, -1.0f - j, -1); // 1, -1, -1
			glVertex3f(-1.0f + i, -1.0f - j, -1); // -1, -1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, -1.0f - j, -1); // -1, -1, -1
			glVertex3f(-1.0f + i, 1.0f - j, -1); // -1, 1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f + i, 1.0f - j, -1); // -1, 1, -1
			glVertex3f(1.0f + i, 1.0f - j, -1); // 1, 1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(1.0f + i, 1.0f - j, -1); // 1, 1, -1
			glVertex3f(1.0f + i, -1.0f - j, -1); // 1, -1, -1
			glEnd();
		}
	}


	for (double i = 0; i < size.getZSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			glBegin(GL_LINES);
			glColor3f(1.0f, 0, 1.0f);
			glVertex3f(-1.0f, 1.0f - i, 1.0f + j); // -1, 1, 1
			glVertex3f(-1.0f, 1.0f - i, -1.0f + j); // -1, 1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f, 1.0f - i, -1.0f + j); // -1, 1, -1
			glVertex3f(-1.0f, -1.0f - i, -1.0f + j); // -1, -1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f, -1.0f - i, -1.0f + j); // -1, -1, -1
			glVertex3f(-1.0f, -1.0f - i, 1.0f + j); // -1, -1, 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(-1.0f, -1.0f - i, 1.0f + j); // -1, -1, 1
			glVertex3f(-1.0f, 1.0f - i, 1.0f + j); // -1, 1, 1
			glEnd();
		}
	}


	for (double i = 0; i < size.getZSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			glBegin(GL_LINES);
			glColor3f(0, 1.0f, 1.0f);
			glVertex3f(size.getXSize() + 1, 1.0f - i, -1.0f + j); // xSize + 1, 1, -1
			glVertex3f(size.getXSize() + 1, 1.0f - i, 1.0f + j); // xSize + 1, 1, 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(size.getXSize() + 1, 1.0f - i, 1.0f + j); // xSize + 1, 1, 1
			glVertex3f(size.getXSize() + 1, -1.0f - i, 1.0f + j); // xSize + 1, -1, 1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(size.getXSize() + 1, -1.0f - i, 1.0f + j); // xSize + 1, -1, 1
			glVertex3f(size.getXSize() + 1, -1.0f - i, -1.0f + j); // xSize + 1, -1, -1
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(size.getXSize() + 1, -1.0f - i, -1.0f + j); // xSize + 1, -1, -1
			glVertex3f(size.getXSize() + 1, 1.0f - i, -1.0f + j); // xSize + 1, 1, -1
			glEnd();
		}
	}
	glEnd();
}

void GraphicsUtility::drawBuilding(Point3D& coordinate, Size& size, Color *color, 
								   double polygonPeriod, int buildingIndex, BuildingTexture textures) {
	int currentNormal = 0;
	
	Normal *normals = buildingNormals.at(buildingIndex);

	// Calculate texel periods for each axis...
	float texCoordPeriodX = 1.0 / (size.getXSize() / polygonPeriod);
	float texCoordPeriodY = 1.0 / (size.getYSize() / polygonPeriod);
	float texCoordPeriodZ = 1.0 / (size.getZSize() / polygonPeriod);

	if (color != NULL) {
		glColor3f(color->getR(), color->getG(), color->getB());
	}
		
	/*-------------------------------------- FRONT WALL OF BUILDING ------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getFrontWallTexture());
	}

	glBegin(GL_QUADS);

	for (double i = 0, int texelIndexX = 0; i < size.getXSize(); i += polygonPeriod, ++texelIndexX) {
		for (double j = 0, int texelIndexY = 0; j < size.getYSize(); j += polygonPeriod, ++texelIndexY) {
			double xFirst =  texelIndexY * texCoordPeriodY;
			double xSecond = (texelIndexY + 1) * texCoordPeriodY;
			double yFirst =  texelIndexX * texCoordPeriodX;
			double ySecond = (texelIndexX + 1) * texCoordPeriodX;
			 
			glNormal3f(normals[currentNormal].x, normals[currentNormal].y, normals[currentNormal].z);
			glTexCoord2d(xFirst, yFirst);
			glVertex3d(1.0f + i + coordinate.getX(), 1.0f + coordinate.getY(), -1.0f + j + coordinate.getZ()); // 1, 1, -1
			glTexCoord2d(xSecond, yFirst);
			glVertex3d(-1.0f + i + coordinate.getX(), 1.0f + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, 1, -1
		    glTexCoord2d(xSecond, ySecond);
			glVertex3d(-1.0f + i + coordinate.getX(), 1.0f + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, 1, 1
			glTexCoord2d(xSecond, yFirst);
			glVertex3d(1.0f + i + coordinate.getX(), 1.0f + coordinate.getY(), 1.0f + j + coordinate.getZ()); // 1, 1, 1
			++currentNormal;
		}
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
	/*------------------------------------------------------------------------------------------------------*/

	/*---------------------------------------- BACK WALL OF BUILDING ---------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getBackWallTexture());
	}
	
	glBegin(GL_QUADS);

	for (double i = 0, int texelIndexX = 0; i < size.getXSize(); i += polygonPeriod, ++texelIndexX) {
		for (double j = 0, int texelIndexY = 0; j < size.getYSize(); j += polygonPeriod, ++texelIndexY) {
			double xFirst = texelIndexY * texCoordPeriodY;
			double xSecond = (texelIndexY + 1) * texCoordPeriodY;
			double yFirst = texelIndexX * texCoordPeriodX;
			double ySecond = (texelIndexX + 1) * texCoordPeriodX;

			glNormal3f(normals[currentNormal].x, normals[currentNormal].y, normals[currentNormal].z);
			glTexCoord2f(xSecond, ySecond);
			glVertex3f(1.0f + i + coordinate.getX(), -size.getZSize() - 1 + coordinate.getY(), 1.0 + j + coordinate.getZ()); // 1, -zSize - 1, 1
			glTexCoord2f(xSecond, yFirst);
			glVertex3f(-1.0f + i + coordinate.getX(), -size.getZSize() - 1 + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, -zSize - 1, 1
			glTexCoord2f(xFirst, yFirst);
			glVertex3f(-1.0f + i + coordinate.getX(), -size.getZSize() - 1 + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, -zSize - 1, -1
			glTexCoord2f(xFirst, yFirst);	
			glVertex3f(1.0f + i + coordinate.getX(), -size.getZSize() - 1 + coordinate.getY(), -1.0f + j + coordinate.getZ()); // 1, -zSize - 1, -1
			++currentNormal;
		}
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
	/*------------------------------------------------------------------------------------------------------*/

	/*----------------------------------- RIGHT WALL OF BUILDING -------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getRightWallTexture());
	}

	glBegin(GL_QUADS);
	
	for (double i = 0, int texelIndexX = 0; i < size.getXSize(); i += polygonPeriod, ++texelIndexX) {
		for (double j = 0, int texelIndexZ = 0; j < size.getZSize(); j += polygonPeriod, ++texelIndexZ) {
			double xFirst = texelIndexZ * texCoordPeriodZ;
			double xSecond = (texelIndexZ + 1) * texCoordPeriodZ;
			double yFirst = texelIndexX * texCoordPeriodX;
			double ySecond = (texelIndexX + 1) * texCoordPeriodX;

			glNormal3f(normals[currentNormal].x, normals[currentNormal].y, normals[currentNormal].z);
			glTexCoord2f(xFirst, yFirst);
			glVertex3f(1.0f + i + coordinate.getX(), 1.0f - j + coordinate.getY(), size.getYSize() + 1 + coordinate.getZ()); // 1, 1, ySize + 1
			glTexCoord2f(xFirst, ySecond);
			glVertex3f(-1.0f + i + coordinate.getX(), 1.0f - j + coordinate.getY(), size.getYSize() + 1 + coordinate.getZ()); // -1, 1, ySize + 1
			glTexCoord2f(xSecond, ySecond);
			glVertex3f(-1.0f + i + coordinate.getX(), -1.0f - j + coordinate.getY(), size.getYSize() + 1 + coordinate.getZ()); // -1, -1, ySize + 1
			glTexCoord2f(xSecond, yFirst);
			glVertex3f(1.0f + i + coordinate.getX(), -1.0f - j + coordinate.getY(), size.getYSize() + 1 + coordinate.getZ()); // 1, -1, ySize + 1
			++currentNormal;
		}
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
	/*------------------------------------------------------------------------------------------------------*/

	/*-------------------------------------- LEFT WALL OF BUILDING ------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getLeftWallTexture());
	}

	glBegin(GL_QUADS);

	for (double i = 0, int texelIndexX = 0; i < size.getXSize(); i += polygonPeriod, ++texelIndexX) {
		for (double j = 0, int texelIndexZ = 0; j < size.getZSize(); j += polygonPeriod, ++texelIndexZ) {
			double xFirst = texelIndexZ * texCoordPeriodZ;
			double xSecond = (texelIndexZ + 1) * texCoordPeriodZ;
			double yFirst = texelIndexX * texCoordPeriodX;
			double ySecond = (texelIndexX + 1) * texCoordPeriodX;

			glNormal3f(normals[currentNormal].x, normals[currentNormal].y, normals[currentNormal].z);
			glTexCoord2f(xFirst, yFirst);
			glVertex3f(1.0f + i + coordinate.getX(), -1.0f - j + coordinate.getY(), -1 + coordinate.getZ()); // 1, -1, -1
			glTexCoord2f(xFirst, ySecond);
			glVertex3f(-1.0f + i + coordinate.getX(), -1.0f - j + coordinate.getY(), -1 + coordinate.getZ()); // -1, -1, -1
			glTexCoord2f(xSecond, ySecond);
			glVertex3f(-1.0f + i + coordinate.getX(), 1.0f - j + coordinate.getY(), -1 + coordinate.getZ()); // -1, 1, -1
			glTexCoord2f(xSecond, yFirst);
			glVertex3f(1.0f + i + coordinate.getX(), 1.0f - j + coordinate.getY(), -1 + coordinate.getZ()); // 1, 1, -1
			++currentNormal;
		}
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
	/*------------------------------------------------------------------------------------------------------*/

	/*------------------------------------------- DOWN WALL OF BUILDING -------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getDownWallTexture());
	}

	glBegin(GL_QUADS);

	for (double i = 0, int texelIndexZ = 0; i < size.getZSize(); i += polygonPeriod, ++texelIndexZ) {
		for (double j = 0, int texelIndexY = 0; j < size.getYSize(); j += polygonPeriod, ++texelIndexY) {
			double xFirst = texelIndexY * texCoordPeriodY;
			double xSecond = (texelIndexY + 1) * texCoordPeriodY;
			double yFirst = texelIndexZ * texCoordPeriodZ;
			double ySecond = (texelIndexZ + 1) * texCoordPeriodZ;

			glNormal3f(normals[currentNormal].x, normals[currentNormal].y, normals[currentNormal].z);
			glTexCoord2f(xFirst, yFirst);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f - i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, 1, 1
			glTexCoord2f(xFirst, ySecond);
			glVertex3f(-1.0f + coordinate.getX(), 1.0f - i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, 1, -1
			glTexCoord2f(xSecond, ySecond);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f - i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // -1, -1, -1
			glTexCoord2f(xSecond, yFirst);
			glVertex3f(-1.0f + coordinate.getX(), -1.0f - i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // -1, -1, 1
			++currentNormal;
		}
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
	/*------------------------------------------------------------------------------------------------------*/

	/*------------------------------------- UP WALL OF BUILDING --------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getUpWallTexture());
	}

	glBegin(GL_QUADS);

	for (double i = 0, int texelIndexZ = 0; i < size.getZSize(); i += polygonPeriod, ++texelIndexZ) {
		for (double j = 0, int texelIndexY = 0; j < size.getYSize(); j += polygonPeriod, ++texelIndexY) {
			double xFirst = texelIndexY * texCoordPeriodY;
			double xSecond = (texelIndexY + 1) * texCoordPeriodY;
			double yFirst = texelIndexZ * texCoordPeriodZ;
			double ySecond = (texelIndexZ + 1) * texCoordPeriodZ;

			glNormal3f(normals[currentNormal].x, normals[currentNormal].y, normals[currentNormal].z);
			glTexCoord2f(xFirst, yFirst);
			glVertex3f(size.getXSize() + 1 + coordinate.getX(), 1.0f - i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // xSize + 1, 1, -1
			glTexCoord2f(xFirst, ySecond);
			glVertex3f(size.getXSize() + 1 + coordinate.getX(), 1.0f - i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // xSize + 1, 1, 1
			glTexCoord2f(xSecond,ySecond);
			glVertex3f(size.getXSize() + 1 + coordinate.getX(), -1.0f - i + coordinate.getY(), 1.0f + j + coordinate.getZ()); // xSize + 1, -1, 1
			glTexCoord2f(xSecond,yFirst);
			glVertex3f(size.getXSize() + 1 + coordinate.getX(), -1.0f - i + coordinate.getY(), -1.0f + j + coordinate.getZ()); // xSize + 1, -1, -1
			++currentNormal;
		}
	}
	
	glEnd();

	glDisable(GL_TEXTURE_2D);
	/*------------------------------------------------------------------------------------------------------*/
}

void GraphicsUtility::drawBuildingStandard(Point3D& coordinate, Size& size, Color *color, int buildingIndex, BuildingTexture textures) {
	Normal* normals = buildingNormals.at(buildingIndex);

	/*---------------------------- FRONT WALL OF BUILDING ---------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getFrontWallTexture());
	}

	glBegin(GL_QUADS);
	glNormal3f(normals[0].x, normals[0].y, normals[0].z);
	glTexCoord2d(0,0);
	glVertex3f(size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(0,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,0);
	glVertex3f(size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*---------------------------------------------------------------------------------------------------*/

	/*------------------------------- BACK WALL OF BUILDING ---------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getBackWallTexture());
	}

	glBegin(GL_QUADS);
	glNormal3f(normals[1].x, normals[1].y, normals[1].z);
	glTexCoord2d(0,0);
	glVertex3f(size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(0,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,0);
	glVertex3f(size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*---------------------------------------------------------------------------------------------------*/

	/*---------------------------------------- LEFT WALL OF BUILDING ------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getLeftWallTexture());
	}

	glBegin(GL_QUADS);
	glNormal3f(normals[2].x, normals[2].y, normals[2].z);
	glTexCoord2d(0,0);
	glVertex3f(size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(0,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,0);
	glVertex3f(size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*---------------------------------------------------------------------------------------------------*/

	/*--------------------------------------- RIGHT WALL OF BUILDING ------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getRightWallTexture());
	}

	glBegin(GL_QUADS);
	glNormal3f(normals[3].x, normals[3].y, normals[3].z);
	glTexCoord2d(0,0);
	glVertex3f(size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(0,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,0);
	glVertex3f(size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*---------------------------------------------------------------------------------------------------*/
	
	

	/*----------------------------------- DOWN WALL OF BUILDING ------------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getDownWallTexture());
	}

	glBegin(GL_QUADS);
	glNormal3f(normals[4].x, normals[4].y, normals[4].z);
	glTexCoord2d(0,0);
	glVertex3f(-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(0,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,1);
	glVertex3f(-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,0);
	glVertex3f(-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*---------------------------------------------------------------------------------------------------*/

	/*--------------------------------------- UP WALL OF BUILDING --------------------------------------*/
	if (color == NULL) {
		enableTexture(textures.getUpWallTexture());
	}

	glBegin(GL_QUADS);
	glNormal3f(normals[5].x, normals[5].y, normals[5].z);
	glTexCoord2d(0,0);
	glVertex3f(size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glTexCoord2d(0,1);
	glVertex3f(size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,1);
	glVertex3f(size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ());
	glTexCoord2d(1,0);
	glVertex3f(size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ());
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*---------------------------------------------------------------------------------------------------*/
}

void GraphicsUtility::computeNormals(Point3D& coordinate, Size& size, double polygonPeriod, int geometry) {
	vector<Surface> surfaces;

	vector<vector<Normal>> normalPacks;
	vector<vector<Surface>> surfacePacks;
	Surface* gpuSurfacesPt;
	Normal* gpuNormalPt;
	Normal* normalPt;

	switch (geometry) {
	case GraphicsParameters::GEO_GROUND:
		fillGroundSurfaces(surfaces, coordinate, size, polygonPeriod);
	case GraphicsParameters::GEO_BUILDING:
		fillCubeSurfaces(surfaces, coordinate, size, polygonPeriod);
	case GraphicsParameters::GEO_BUILDING_STANDARD:
		fillCubeSurfacesStandard(surfaces, coordinate, size);
	}

	// Creating packs for surfaces
	for (int i = 0; i < (surfaces.size() / GraphicsParameters::MAX_THREAD) + 1; ++i) {
		surfacePacks.push_back(vector<Surface>());
	}

	// Fill surface packs
	for (int i = 0, currentPackIndex = 0; i < surfaces.size(); ++i) {
		if (surfacePacks.at(currentPackIndex).size() == GraphicsParameters::MAX_THREAD) {
			++currentPackIndex;
		}

		surfacePacks.at(currentPackIndex).push_back(surfaces.at(i));
	}

	// Compute normals for each surface packs 
	for (int i = 0; i < surfacePacks.size(); ++i) {
		vector<Surface> currentSurfacePack = surfacePacks.at(i);
		Surface* currentSurfacePackPt = new Surface[currentSurfacePack.size()];
		vector<Normal> currentNormalPack;
		Normal* currentNormalPackPt = new Normal[currentSurfacePack.size()];

		for (int j = 0; j < currentSurfacePack.size(); ++j) {
			currentSurfacePackPt[j] = currentSurfacePack.at(j);
		}

		cudaSetDevice(0);

		cudaMalloc((void **)&gpuSurfacesPt, currentSurfacePack.size() * sizeof(Surface));
		cudaMalloc((void **)&gpuNormalPt, currentSurfacePack.size() * sizeof(Normal));
		cudaMemcpy(gpuSurfacesPt, currentSurfacePackPt, currentSurfacePack.size() * sizeof(Surface), cudaMemcpyHostToDevice);

		normalCalculator<<<currentSurfacePack.size(), 1>>>(gpuSurfacesPt, gpuNormalPt);

		cudaDeviceSynchronize();
		cudaMemcpy(currentNormalPackPt, gpuNormalPt, currentSurfacePack.size() * sizeof(Normal), cudaMemcpyDeviceToHost);
		cudaDeviceReset();
		cudaFree(gpuSurfacesPt);
		cudaFree(gpuNormalPt);

		for (int j = 0; j < currentSurfacePack.size(); ++j) {
			currentNormalPack.push_back(currentNormalPackPt[j]);
		}

		normalPacks.push_back(currentNormalPack);

		delete[](currentSurfacePackPt);
		delete[](currentNormalPackPt);
	}

	// Retrieve and save surface normals
	normalPt = new Normal[surfaces.size()];

	int index = 0;

	for (int i = 0; i < normalPacks.size(); ++i) {
		vector<Normal> currentNormalPack = normalPacks.at(i);

		for (int j = 0; j < currentNormalPack.size(); ++j, ++index) {
			normalPt[index] = currentNormalPack.at(j);
		}
	}

	buildingNormals.push_back(normalPt);
}

void GraphicsUtility::fillCubeSurfaces(vector<Surface>& surfaces, Point3D& coordinate, Size& size, double polygonPeriod) {
	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, i + coordinate.getX(), 0 + coordinate.getY(), j + coordinate.getZ(), 1, 1, -1);
			fillVertex(second, i + coordinate.getX(), 0 + coordinate.getY(), j + coordinate.getZ(), -1, 1, -1);
			fillVertex(third, i + coordinate.getX(), 0 + coordinate.getY(), j + coordinate.getZ(), -1, 1, 1);
			fillVertex(fourth, i + coordinate.getX(), 0 + coordinate.getY(), j + coordinate.getZ(), 1, 1, 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}


	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, i + coordinate.getX(), 0 + coordinate.getY(), j, 1 + coordinate.getZ(), -size.getZSize() - 1, 1);
			fillVertex(second, i + coordinate.getX(), 0 + coordinate.getY(), j, -1 + coordinate.getZ(), -size.getZSize() - 1, 1);
			fillVertex(third, i + coordinate.getX(), 0 + coordinate.getY(), j, -1 + coordinate.getZ(), -size.getZSize() - 1, -1);
			fillVertex(fourth, i + coordinate.getX(), 0 + coordinate.getY(), j, 1 + coordinate.getZ(), -size.getZSize() - 1, -1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}


	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getZSize(); j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), 1, 1, size.getYSize() + 1);
			fillVertex(second, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), -1, 1, size.getYSize() + 1);
			fillVertex(third, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), -1, -1, size.getYSize() + 1);
			fillVertex(fourth, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), 1, -1, size.getYSize() + 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}


	for (double i = 0; i < size.getXSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getZSize(); j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), 1, -1, -1);
			fillVertex(second, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), -1, -1, -1);
			fillVertex(third, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), -1, 1, -1);
			fillVertex(fourth, i + coordinate.getX(), -j + coordinate.getY(), 0 + coordinate.getZ(), 1, 1, -1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}


	for (double i = 0; i < size.getZSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, 1, 1);
			fillVertex(second, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, 1, -1);
			fillVertex(third, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, -1, -1);
			fillVertex(fourth, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, -1, 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}


	for (double i = 0; i < size.getZSize(); i += polygonPeriod) {
		for (double j = 0; j < size.getYSize(); j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), size.getXSize() + 1, 1, -1);
			fillVertex(second, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), size.getXSize() + 1, 1, 1);
			fillVertex(third, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), size.getXSize() + 1, -1, 1);
			fillVertex(fourth, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), size.getXSize() + 1, -1, -1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}
}

void GraphicsUtility::fillCubeSurfacesStandard(vector<Surface>& surfaces, Point3D& coordinate, Size& size) {
	Surface frontSurface, backSurface, leftSurface, rightSurface, upSurface, downSurface;
	Vertex first, second, third, fourth;

	/*---------------------------- FRONT WALL OF BUILDING ---------------------------------------------*/
	fillVertex(first,size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(),0,0,0);
	fillVertex(second,-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(),0, 0, 0);
	fillVertex(third,-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(),0, 0, 0);
	fillVertex(fourth,size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillSurface(frontSurface, first, second, third, fourth);
	surfaces.push_back(frontSurface);
	/*---------------------------------------------------------------------------------------------------*/

	/*------------------------------- BACK WALL OF BUILDING ---------------------------------------------*/
	fillVertex(first,size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(),0, 0, 0);
	fillVertex(second,-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(),0, 0, 0);
	fillVertex(third,-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(fourth, size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillSurface(backSurface, first, second, third, fourth);
	surfaces.push_back(backSurface);
	/*---------------------------------------------------------------------------------------------------*/

	/*---------------------------------------- LEFT WALL OF BUILDING ------------------------------------*/
	fillVertex(first,size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(second,-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(third,-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(fourth,size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillSurface(leftSurface, first, second, third, fourth);
	surfaces.push_back(leftSurface);
	/*---------------------------------------------------------------------------------------------------*/

	/*--------------------------------------- RIGHT WALL OF BUILDING ------------------------------------*/
	fillVertex(first,size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(second,-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(third,-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(fourth,size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillSurface(rightSurface, first, second, third, fourth);
	surfaces.push_back(rightSurface);
	/*---------------------------------------------------------------------------------------------------*/

	/*----------------------------------- DOWN WALL OF BUILDING ------------------------------------------*/
	fillVertex(first,-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(second,-size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(third,-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(fourth,-size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillSurface(downSurface, first, second, third, fourth);
	surfaces.push_back(downSurface);
	/*---------------------------------------------------------------------------------------------------*/

	/*--------------------------------------- UP WALL OF BUILDING --------------------------------------*/
	fillVertex(first,size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(second,size.getXSize() + coordinate.getX(), size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillVertex(third,size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), size.getZSize() + coordinate.getZ(),0, 0, 0);
	fillVertex(fourth,size.getXSize() + coordinate.getX(), -size.getYSize() + coordinate.getY(), -size.getZSize() + coordinate.getZ(), 0, 0, 0);
	fillSurface(upSurface, first, second, third, fourth);
	surfaces.push_back(upSurface);
	/*---------------------------------------------------------------------------------------------------*/
}

void GraphicsUtility::fillGroundSurfaces(vector<Surface>& surfaces, Point3D& coordinate, Size& size, double polygonPeriod) {
	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, 1, 1);
			fillVertex(second, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, 1, -1);
			fillVertex(third, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, -1, -1);
			fillVertex(fourth, 0 + coordinate.getX(), -i + coordinate.getY(), j + coordinate.getZ(), -1, -1, 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, 0 + coordinate.getX(), i + coordinate.getY(), j + coordinate.getZ(), -1, 1, 1);
			fillVertex(second, 0 + coordinate.getX(), i + coordinate.getY(), j + coordinate.getZ(), -1, 1, -1);
			fillVertex(third, 0 + coordinate.getX(), i + coordinate.getY(), j + coordinate.getZ(), -1, -1, -1);
			fillVertex(fourth, 0 + coordinate.getX(), i + coordinate.getY(), j + coordinate.getZ(), -1, -1, 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, 0 + coordinate.getX(), -i + coordinate.getY(), -j + coordinate.getZ(), -1, 1, 1);
			fillVertex(second, 0 + coordinate.getX(), -i + coordinate.getY(), -j + coordinate.getZ(), -1, 1, -1);
			fillVertex(third, 0 + coordinate.getX(), -i + coordinate.getY(), -j + coordinate.getZ(), -1, -1, -1);
			fillVertex(fourth, 0 + coordinate.getX(), -i + coordinate.getY(), -j + coordinate.getZ(), -1, -1, 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}

	for (double i = 0; i < size.getXSize() / 2; i += polygonPeriod) {
		for (double j = 0; j < size.getYSize() / 2; j += polygonPeriod) {
			Surface surface;
			Vertex first, second, third, fourth;
			fillVertex(first, 0 + coordinate.getX(), i + coordinate.getY(), -j + coordinate.getZ(), -1, 1, 1);
			fillVertex(second, 0 + coordinate.getX(), i + coordinate.getY(), -j + coordinate.getZ(), -1, 1, -1);
			fillVertex(third, 0 + coordinate.getX(), i + coordinate.getY(), -j + coordinate.getZ(), -1, -1, -1);
			fillVertex(fourth, 0 + coordinate.getX(), i + coordinate.getY(), -j + coordinate.getZ(), -1, -1, 1);
			fillSurface(surface, first, second, third, fourth);
			surfaces.push_back(surface);
		}
	}
}

void GraphicsUtility::fillVertex(Vertex& vertex,double i,double j,double k,int x,int y,int z) {
	vertex.x = x + i;
	vertex.y = y + j;
	vertex.z = z + k;
}

void GraphicsUtility::fillSurface(Surface& surface,Vertex first,Vertex second,Vertex third,Vertex fourth) {
	surface.first = first;
	surface.second = second;
	surface.third = third;
	surface.fourth = fourth;
}

void GraphicsUtility::loadTexture(string texturePath) {
	Texture texture;
	
	texture.textureId = SOIL_load_OGL_texture(texturePath.c_str(), SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA);

	textureImages.push_back(texture);
}

void GraphicsUtility::enableTexture(GLuint textureId) {
	glEnable(GL_TEXTURE_2D);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glBindTexture(GL_TEXTURE_2D, textureId);
}