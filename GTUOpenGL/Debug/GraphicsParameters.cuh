#ifndef CAMERA_PARAMS_H
#define CAMERA_PARAMS_H
#include <GL\freeglut.h>
#include <iostream>

class GraphicsParameters {
public:
	void setInitialCamPosition(double eyeX, double eyeY, double eyeZ, double centerX, double centerY, double centerZ,
							   double upX, double upY, double upZ) {
		this->eyeX = eyeX;
		this->eyeY = eyeY; 
		this->eyeZ = eyeZ;
		this->centerX = centerX;
		this->centerY = centerY;
		this->centerZ = centerZ;
		this->upX = upX;
		this->upY = upY;
		this->upZ = upZ;
	}

	void setLightDiffuse(double r, double g, double b, double a) {
		lightDiffuse[0] = r;
		lightDiffuse[1] = g;
		lightDiffuse[2] = b;
		lightDiffuse[3] = a;
	}

	void setLightAmbient(double r, double g, double b, double a) {
		lightAmbient[0] = r;
		lightAmbient[1] = g;
		lightAmbient[2] = b;
		lightAmbient[3] = a;
	}

	void setLightPosition(double x, double y, double z, double a) {
		lightPosition[0] = x;
		lightPosition[1] = y;
		lightPosition[2] = z;
		lightPosition[3] = a;
	}

	/* --------- Drawing Parameters ------------*/
	static const int MAX_THREAD = 25000;
	static const int GEO_BUILDING = 0;
	static const int GEO_GROUND = 1;
	static const int GEO_BUILDING_STANDARD = 2;
	/* ---------------------------------------- */

	/* ----- Camera Change Parameters -------*/
	std::string CHANGE_MODE_STR;
	int CHANGE_MODE;
	static const int CHANGE_EYE_X = 0;
	static const int CHANGE_EYE_Y = 1;
	static const int CHANGE_EYE_Z = 2;
	static const int CHANGE_CENTER_X = 3;
	static const int CHANGE_CENTER_Y = 4;
	static const int CHANGE_CENTER_Z = 5;
	static const int CHANGE_UP_X = 6;
	static const int CHANGE_UP_Y = 7;
	static const int CHANGE_UP_Z = 8;
	/* -------------------------------- */

	/* ------Camera Parameters------ */
	int eyeX;
	int eyeY;
	int eyeZ;
	int centerX;
	int centerY;
	int centerZ;
	int upX;
	int upY;
	int upZ;
	double texScale = 0.0;
	/* -----------------------------*/

	/* ------------- Light Parameters ---------------*/
	GLfloat lightDiffuse[4];
	GLfloat lightPosition[4];
	GLfloat lightAmbient[4];
	/* ----------------------------------------------*/

	/* ------------ Cloud Parameters ----------------*/
	static const int maxX;
	static const int maxY;
	static const int maxZ;
	static const int minX;
	static const int minY;
	static const int minZ;
	static const int minTextureIndex;
	static const int maxTextureIndex;
	/* ----------------------------------------------*/
};
#endif