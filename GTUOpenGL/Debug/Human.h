#pragma once
class Human
{
public:
	Human(int x, int y, int height, int width) {
		this->x = x;
		this->y = y;
		this->height = height;
		this->width = width;
	}

	~Human() { }

	int x;
	int y;
	int height;
	int width;
};

