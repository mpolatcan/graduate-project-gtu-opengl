#include "GraphicsUtility.cuh"

// TODO Building textures... (Alignment problem...(%90))
// TODO Parallax mapping...
// TODO Clouds... (Cloud drawing completed... Particle systems started)
// TODO Fogs...
// TODO Shadow...
// TODO Reflection...

GraphicsUtility utility;
GraphicsParameters params;
Mesh *mesh;

// ------------------ OPENGL STANDARD FUNCTION PROTOTYPES ----------------------
void init();
void keyboardStandard(unsigned char key, int x, int y);
void keyboardArrows(int key, int x, int y);
void display();
void reshape(int x, int y);
// -----------------------------------------------------------------------------

GLfloat fogColor[4] = { 0.5, 0.5, 0.5, 1.0 };
bool fogEnabled = false;

double xCoor = 0;
double yCoor = 0;
double zCoor = 0;

/* ------------------------- HUMAN DETECTION --------------------------------- */
#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT "27015"

vector<vector<Human>> detectedHumans;
SOCKET ListenSocket;
SOCKET ClientSocket = INVALID_SOCKET;
mutex guard;

bool initSocket();
bool receiveAndParseMessage();
vector<string> splitString(const char *str, char c);
void fetchCoordinates();
/* -------------------------------------------------------------------------- */

int main(int argc, char** argv) {
	utility.prepareNormals();
	cout << "Preparing surface normals...." << endl;

	if (!initSocket())
		return -1;

	thread subscriber(fetchCoordinates);

	cout << "Server initalized succesfully\n" << endl;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_ALPHA);
	glutInitWindowSize(1024, 768);
	glutInitWindowPosition(70, 70);
	glutCreateWindow(argv[0]);
	init();
	mesh = new Mesh("Prisoner_c.obj");
	utility.prepareTextures(); // Preparing textures before program begins...
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboardStandard);
	glutSpecialFunc(keyboardArrows);
	glutMainLoop();
	return 0;
}

void fetchCoordinates() {
	// Accept a client socket
	printf("Accept a client ... ");
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
	}
	printf("A client connected...\n");

	// No longer need server socket
	closesocket(ListenSocket);

	while (true) {
		receiveAndParseMessage();
	}
}

void init()
{
	params.setInitialCamPosition(44, 101, 99, 7.0, 17.0, 2.0, 25.0, 1.0, 0.0);
	params.setLightDiffuse(1.0, 1.0, 1.0, 1.0);
	params.setLightPosition(0.0, 0.0, 0.0, 1.0);

	/* Enable a single OpenGL light */
	glLightfv(GL_LIGHT0, GL_DIFFUSE, params.lightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, params.lightPosition);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void keyboardStandard(unsigned char key, int x, int y) {
	switch (key) {
		/* ----- Camera Position Mode Flags ---- */
	case '1':
		params.CHANGE_MODE = params.CHANGE_EYE_X;
		params.CHANGE_MODE_STR = "Change Eye X";
		break;
	case '2':
		params.CHANGE_MODE = params.CHANGE_EYE_Y;
		params.CHANGE_MODE_STR = "Change Eye Y";
		break;
	case '3':
		params.CHANGE_MODE = params.CHANGE_EYE_Z;
		params.CHANGE_MODE_STR = "Change Eye Z";
		break;
	case '4':
		params.CHANGE_MODE = params.CHANGE_CENTER_X;
		params.CHANGE_MODE_STR = "Change Center X";
		break;
	case '5':
		params.CHANGE_MODE = params.CHANGE_CENTER_Y;
		params.CHANGE_MODE_STR = "Change Center Y";
		break;
	case '6':
		params.CHANGE_MODE = params.CHANGE_CENTER_Z;
		params.CHANGE_MODE_STR = "Change Center Z";
		break;
	case '7':
		params.CHANGE_MODE = params.CHANGE_UP_X;
		params.CHANGE_MODE_STR = "Change Up X";
		break;
	case '8':
		params.CHANGE_MODE = params.CHANGE_UP_Y;
		params.CHANGE_MODE_STR = "Change Up Y";
		break;
	case '9':
		params.CHANGE_MODE = params.CHANGE_UP_Z;
		params.CHANGE_MODE_STR = "Change Up Z";
		break;
	case 'f':
		if (fogEnabled) {
			fogEnabled = false;
		}
		else {
			fogEnabled = true;
		}
		glutPostRedisplay();
		break;
		/* ---------------------------------------*/
	}

	cout << "Change Mode: " << params.CHANGE_MODE_STR.c_str() << endl;

}

void keyboardArrows(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_UP:
		switch (params.CHANGE_MODE) {
		case params.CHANGE_EYE_X:
			++params.eyeX;
			break;
		case params.CHANGE_EYE_Y:
			++params.eyeY;
			break;
		case params.CHANGE_EYE_Z:
			++params.eyeZ;
			break;
		case params.CHANGE_CENTER_X:
			++params.centerX;
			break;
		case params.CHANGE_CENTER_Y:
			++params.centerY;
			break;
		case params.CHANGE_CENTER_Z:
			++params.centerZ;
			break;
		case params.CHANGE_UP_X:
			++xCoor;
			break;
		case params.CHANGE_UP_Y:
			++yCoor;
			break;
		case params.CHANGE_UP_Z:
			++zCoor;
			break;
		}
		glutPostRedisplay();
		break;

	case GLUT_KEY_DOWN:
		switch (params.CHANGE_MODE) {
		case params.CHANGE_EYE_X:
			--params.eyeX;
			break;
		case params.CHANGE_EYE_Y:
			--params.eyeY;
			break;
		case params.CHANGE_EYE_Z:
			--params.eyeZ;
			break;
		case params.CHANGE_CENTER_X:
			--params.centerX;
			break;
		case params.CHANGE_CENTER_Y:
			--params.centerY;
			break;
		case params.CHANGE_CENTER_Z:
			--params.centerZ;
			break;
		case params.CHANGE_UP_X:
			--xCoor;
			break;
		case params.CHANGE_UP_Y:
			--yCoor;
			break;
		case params.CHANGE_UP_Z:
			--zCoor;
			break;
		}
		glutPostRedisplay();
		break;
	}

	/*
	cout << "Cam position: " << endl;
	cout << "Eye x-y-z: " << params.eyeX << "-" << params.eyeY << "-" << params.eyeZ << endl;
	cout << "Center x-y-z" << params.centerX << "-" << params.centerY << "-" << params.centerZ << endl;
	*/
}

void reshape(int x, int y)
{
	if (y == 0 || x == 0) return;

	glViewport(0, 0, x, y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(67.0, (GLdouble)x / (GLdouble)y, 1, 1000);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(params.eyeX, params.eyeY, params.eyeZ,
		params.centerX, params.centerY, params.centerZ,
		params.upX, params.upY, params.upZ);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(params.eyeX, params.eyeY, params.eyeZ,
		params.centerX, params.centerY, params.centerZ,
		params.upX, params.upY, params.upZ);
	
	if (fogEnabled) {
		glFogi(GL_FOG_MODE, GL_LINEAR);
		glHint(GL_FOG_HINT, GL_DONT_CARE);
		glFogfv(GL_FOG_COLOR, fogColor);
		glFogf(GL_FOG_DENSITY, 0.15);
		glFogf(GL_FOG_START, 80.0);
		glFogf(GL_FOG_END, 100.0);
		glEnable(GL_FOG);
	}
	else {
		glDisable(GL_FOG);
	}

	vector<Human>humans;
	guard.lock();
	if (detectedHumans.size() != 0)
	{
		humans = detectedHumans[detectedHumans.size() - 1];
		detectedHumans.clear();
	}

	guard.unlock();

	utility.drawBuildingStandard(Point3D(14, -83, -57), Size(15, 20, 40), NULL, 0, BuildingTexture(utility,1, 2, 3, 3, 22, 0));

	utility.drawBuildingStandard(Point3D(14, -83, 90), Size(15, 20, 40), NULL, 1, BuildingTexture(utility, 4, 5, 6, 7, 22, 0));

	utility.drawBuildingStandard(Point3D(14, 180, -62), Size(15, 20, 40), NULL, 2, BuildingTexture(utility, 8, 11, 13, 0, 22, 0));
	
	utility.drawBuildingStandard(Point3D(14, 158, -41), Size(15, 40, 20), NULL, 3, BuildingTexture(utility, 12, 13, 12, 10, 22, 0));
	
	utility.drawBuildingStandard(Point3D(14, 280, -62), Size(15, 20, 40), NULL, 4, BuildingTexture(utility, 15, 14, 17, 17, 22, 0));

	utility.drawBuildingStandard(Point3D(14, 420, -20), Size(15, 20, 40), NULL, 5, BuildingTexture(utility, 18, 19, 20, 21, 22, 0));

	utility.drawGround(Size(40, 130), Point3D(0, -82, 135), 1, 0, 6);

	utility.drawGround(Size(7, 80), Point3D(0, -107, 90), 1, 0, 7);

	utility.drawGround(Size(45, 230), Point3D(0, -43, 90), 1, 23, 8);
	
	utility.drawGround(Size(35, 300), Point3D(0, 233, 51), 1, 23, 9);

	utility.drawGround(Size(550, 40), Point3D(0, 190, 220), 1, 23, 10);
	
	utility.drawGround(Size(35, 300), Point3D(0, 447, 50), 1, 23, 11);

	utility.drawGround(Size(550, 40), Point3D(0, 190, -120), 1, 23, 12);

	utility.drawGround(Size(240, 200), Point3D(0, 99, 100), 1, 0, 13);

	utility.drawGround(Size(130, 80), Point3D(0, 366, -60), 1, 0, 14);

	utility.drawGround(Size(200, 200), Point3D(0, 350, 119), 1, 0, 15);

	utility.drawGround(Size(550, 40), Point3D(0, 171, 0), 1, 23, 16);

	utility.drawGround(Size(40, 80), Point3D(0, -43, -60), 1, 23, 17);

	utility.drawGround(Size(200, 80), Point3D(0, 50, -60), 1, 0, 18);

	utility.drawGround(Size(10, 80), Point3D(0, 256, -60), 1, 0, 19);

	glScalef(0.48f, 0.48f, 0.48f);
	glRotatef(-90, 0, 0, 1);
	glTranslatef(0, 14, 0);

	for (int i = 0; i < humans.size(); ++i) {
		glTranslatef((double)humans.at(i).y / 5.0, 0, (double)humans.at(i).x / 5.0);
		mesh->render();
	}
		

	glFlush();

	glutSwapBuffers();
	glutPostRedisplay();
}

bool initSocket() {
	WSADATA wsaData;
	int iResult;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	printf("Initialize Winsock .... ");
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		cin >> iResult;
		return false;
	}
	printf(" Succesfull\n");

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	printf("Resolve the server address and port ... ");
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		cin >> iResult;
		return false;
	}
	printf(" Succesfull\n");

	// Create a SOCKET for connecting to server
	printf("Create a SOCKET for connecting to server ... ");
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		cin >> iResult;
		return false;
	}
	printf(" Succesfull\n");

	// Setup the TCP listening socket
	printf("Setup the TCP listening socket .... ");
	::bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		cin >> iResult;
		return false;
	}
	printf("Succesfull\n");

	freeaddrinfo(result);
	printf("Listen socket ... ");
	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		cin >> iResult;
		return false;
	}
	printf("Succesfull \n");	

	return true;
}

bool receiveAndParseMessage() {

	int iResult;
	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;
	vector<Human> humans;

	memset(recvbuf, 0, DEFAULT_BUFLEN);
	printf("Receiving message ... \n");
	iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
	if (iResult > 0) {
		vector<string>datas;
		vector<string>temps;
		// Sample string : 30 50 60 10,10 20 30 40 
		//printf("Received message: %s\n", recvbuf);
		datas = splitString(recvbuf, ',');
		for (int i = 0; i < datas.size(); i++)
		{
			temps = splitString(datas[i].c_str(), ' ');
			if (temps.size() == 4)
				humans.push_back(Human(atoi(temps[0].c_str()), atoi(temps[1].c_str()), atoi(temps[2].c_str()), atoi(temps[3].c_str())));
		}

		printf("Total humans in given message : %d", humans.size());

		guard.lock();
		detectedHumans.push_back(humans);
		guard.unlock();

		strcpy(recvbuf, "ACK");
		iSendResult = send(ClientSocket, recvbuf, iResult, 0);
		if (iSendResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return false;
		}

		//printf("Sended ACK\n");

	}
	else if (iResult == 0)
		printf("Connection closing...\n");
	else {
		printf("recv failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return false;
	}

	return true;
}

vector<string> splitString(const char *str, char c = ' ')
{
	vector<string> result;

	do
	{
		const char *begin = str;

		while (*str != c && *str)
			str++;

		result.push_back(string(begin, str));
	} while (0 != *str++);

	return result;
}