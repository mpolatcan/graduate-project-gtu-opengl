#ifndef GRAPHICS_UTILITY_H
#define GRAPHICS_UTILITY_H
#include <iostream>
#include <vector>
#include <sstream>
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <iomanip>
#include <gl\glew.h>
#include <GL\freeglut.h>
#include <GLFW\glfw3.h>
#include <SOIL.h>
#include <thread>
#include <mutex>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "GraphicsParameters.cuh"
#include "Mesh.h"
#include "Human.h"

using namespace std;

class BuildingTexture;

struct Vertex {
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Surface {
	Vertex first;
	Vertex second;
	Vertex third;
	Vertex fourth;
};

struct Normal {
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct Texture {
	GLuint textureId;
};

class Color {
public: 
	Color(double r, double g, double b) : valR(r), valG(g), valB(b) {}
	double getR() const { return valR;  }
	double getG() const { return valG; }
	double getB() const { return valB; }
private:
	double valR;
	double valG;
	double valB;
};

class Point3D {
public:
	Point3D(double coorX, double coorY, double coorZ) : x(coorX), y(coorY), z(coorZ) {}
	double getX() const { return x; }
	double getY() const { return y; }
	double getZ() const { return z; }
private:
	double x;
	double y;
	double z;
};

class Size {
public:
	Size(double sizeX, double sizeY) : xSize(sizeX), ySize(sizeY) { }
	Size(double sizeX, double sizeY, double sizeZ) : xSize(sizeX), ySize(sizeY), zSize(sizeZ) { }
	double getXSize() const { return xSize; }
	double getYSize() const { return ySize; }
	double getZSize() const { return zSize; }
private:
	double xSize;
	double ySize;
	double zSize;
};

class GraphicsUtility {
	public: 
		void prepareNormals();
		void prepareTextures();
		void computeNormals(Point3D& coordinate, 
							Size& size,
							double polygonPeriod,
							int geometry);
		void drawOutline(Point3D& coordinate,
						 Size& size,
						 Color *color,
						 double polygonPeriod);
		void drawGround(Size& size,
						Point3D& coordinate,
						double polygonPeriod,
						int textureIndex,
						int groundIndex);
		void drawBuilding(Point3D& coordinate,
						  Size& size,
						  Color *color,
						  double polygonPeriod,
						  int buildingIndex,
						  BuildingTexture textures);
		void GraphicsUtility::drawBuildingStandard(Point3D& coordinate,
												   Size& size,
												   Color *color,
												   int buildingIndex,
												   BuildingTexture textures);
		void enableTexture(GLuint textureId);
		void loadTexture(string texturePath);
		vector<Texture> getTextures() const { return textureImages; }
	private:
		void fillCubeSurfaces(vector<Surface>& surfaces, 
							  Point3D& coordinate,
							  Size& size,
							  double polygonPeriod);
		void fillCubeSurfacesStandard(vector<Surface>& surfaces,
									  Point3D& coordinate,
									  Size& size);
		void fillGroundSurfaces(vector<Surface>& surfaces, 
								Point3D& coordinate,
								Size& size,
								double polygonPeriod);
		void fillSurface(Surface& surface, 
						 Vertex first, 
						 Vertex second, 
						 Vertex third, 
						 Vertex fourth);
		void fillVertex(Vertex& vertex, 
					    double i, 
					    double j, 
						double k, 
						int x, 
					    int y, 
						int z);
		vector<Normal*> buildingNormals;
		vector<Texture> textureImages;
};

class BuildingTexture {
public:
	BuildingTexture(GraphicsUtility& utility,
		int frontWallTextureIndex, int backWallTextureIndex,
		int leftWallTextureIndex, int rightWallTextureIndex,
		int upWallTextureIndex, int downWallTextureIndex) {
		frontWallTexture = utility.getTextures().at(frontWallTextureIndex).textureId;
		backWallTexture = utility.getTextures().at(backWallTextureIndex).textureId;
		leftWallTexture = utility.getTextures().at(leftWallTextureIndex).textureId;
		rightWallTexture = utility.getTextures().at(rightWallTextureIndex).textureId;
		upWallTexture = utility.getTextures().at(upWallTextureIndex).textureId;
		downWallTexture = utility.getTextures().at(downWallTextureIndex).textureId;
	}

	GLuint getFrontWallTexture() const { return frontWallTexture; }
	GLuint getBackWallTexture() const { return backWallTexture; }
	GLuint getLeftWallTexture() const { return leftWallTexture; }
	GLuint getRightWallTexture() const { return rightWallTexture; }
	GLuint getUpWallTexture() const { return upWallTexture; }
	GLuint getDownWallTexture() const { return  downWallTexture; }
private:
	GLuint frontWallTexture;
	GLuint backWallTexture;
	GLuint leftWallTexture;
	GLuint rightWallTexture;
	GLuint upWallTexture;
	GLuint downWallTexture;
};
#endif